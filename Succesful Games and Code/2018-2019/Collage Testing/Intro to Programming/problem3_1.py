def problem3_1(txtfilename):
    infile = open(txtfilename, "r")
    data = 0
    for line in infile:
        print(line, end="")
        data += len(line)
    print("")
    print("")
    print("There are", data, "letters in the file.")
    infile.close()
