def problem2_7():
    """ computes area of triangle using Heron's formula. """
    in1 = float(input("enter length of side one:"))
    in2 = float(input("enter length of side two:"))
    in3 = float(input("enter length of side three:"))
    s = (in1 + in2 + in3)/2
    print ("Area of a triangle with sides", in1, in2, in3, "is", (s*(s-in1)*(s-in2) * (s-in3))**.5)
