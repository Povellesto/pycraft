import numpy
import matplotlib.pyplot as plt

timeStep = 10 #Years
waterDepth = 1250#Meters
L = 1336 #Watts/m^2
albedo = 0.33
epsilon = 1
sigma = 5.67E-8 #W/m^2 K^4

heatCapacity = waterDepth * 4.2E6
timeYears = [0]
TK = [0.] #Temperature
heatContent = heatCapacity * TK[0]
heatIn = L * (1 - albedo)/4
heatOut = 0

for itime in range(0, 100):
    print("""






    """)
    timeYears.append (timeStep + timeYears[-1])
    heatOut = epsilon * sigma * pow(TK[-1],4)
    print(timeYears[-1], heatOut, "Time(years), Heat Out")
    heatContent += (heatIn - heatOut) * timeStep * 3.14e7
    print(heatContent, "Heat Content")
    print(heatCapacity, "Heat Capacity")
    TK.append(heatContent / heatCapacity)
plt.plot(timeYears, TK, marker='o')
plt.xlabel("Time (Years)")
plt.ylabel("Temperature (Kelvin)")
plt.title("Naked Planet Time Stepping Model")
plt.text(850,270, "Equilibrium")
plt.text(150,100, "^ Warming UP ^")
plt.show()
