# Defines a "repeat" function that takes 2 arguments.
def repeat(s, exclaim):
    """
    Returns the string 's' repeated 3 times.
    If exclaim is true, add exclamation marks.
    """

    result = s + s + s # can also use "s * 3" which is faster (Why?)
    if exclaim:
        result = result + '!!!'
    return result

def main():
    print repeat('Thomas Rimer', False)
    print repeat('Enerson Poon', True)

"""def main2():
    if name == 'Guido':
        print repeeeet(name) + '!!!' #syntax, Only when a run actually tries to execute the repeeeet() 
        will it notice that there is no such function and raise an error.
    else:
        print repeat(name)"""
main()
