import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.axis as axis
import numpy as np

birddata = pd.read_csv("bird_Tracking.csv")
birdnames = pd.unique(birddata.bird_name)
plt.figure(figsize=(7,7))

for bird_name in birdnames:
    ix = birddata.bird_name == bird_name
    x , y = birddata.longitude[ix], birddata.latitude[ix]
    plt.plot(x,y,".",label=bird_name)

plt.title("Bird Paths Via GPS")
plt.xlabel("Longitude")
plt.ylabel("Latitude")
plt.legend(loc="best")
plt.savefig("3traj.pdf ")
plt.show()
