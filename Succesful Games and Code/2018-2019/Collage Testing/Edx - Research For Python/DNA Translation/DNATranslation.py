"""
Data DNA Translation Code.
NCBI: https://www.ncbi.nlm.nih.gov

Vocab/Information:

    Necliecacids/Necleotides:
        Nucleotides are organic molecules that serve as the monomer units
        for forming the nucleic acid polymers deoxyribonucleic acid (DNA)
        and ribonucleic acid (RNA), both of which are essential biomolecules
        within all life-forms on Earth. Nucleotides are the building blocks of
        nucleic acids; they are composed of three subunit molecules: a nitrogenous base,
        a five-carbon sugar (ribose or deoxyribose), and at least one phosphate group.
        https://en.wikipedia.org/wiki/Nucleotide


    Codons:
        A sequence of three nucleotides that together form a unit of genetic code in
        a DNA or RNA molecule.
        https://www.google.com/search?q=codons&oq=codons&aqs=chrome..69i57j0l5.932j0j9&sourceid=chrome&ie=UTF-8

    Modulo Operator:
        Modulus operator, it is used for remainder division on integers, typically,
        but in Python can be used for floating point numbers. The % (modulo) operator yields
        the remainder from the division of the first argument by the second.
        http://docs.python.org/reference/expressions.html

    Docstring:
        In programming, a docstring is a string literal specified in source code that is used,
        like a comment, to document a specific segment of code. Unlike conventional source code
        comments, or even specifically formatted comments like Javadoc documentation, docstrings
        are not stripped from the source tree when it is parsed, but are retained throughout the
        runtime of the program. This allows the programmer to inspect these comments at run time,
        for instance as an interactive help system, or as metadata.

Goal:
    Translate a string containing a nucleotide sequence into a string containing
    the corresponding sequence of amino acids. Nucleotides are translated in triplets
    using the table dictionary; each amino acid 4 is encoded with a string of length 1.

    We do this by following these steps:

        Step 1: Check that the length of the sequence is divisible by 3

        Step 2: Look up each 3 letter string and store result

        Step 3: Continue lookups until reaching end of sequence

    The Necleotides used in this example came from NCBI (National Center for Biotechnology Information)

"""
class ErrorSequenceNotDivisibleBy3(Exception):
    pass

import csv #

def read_seq(inputfile):
    with open(inputfile, "r") as f:
        seq = f.read() #assigning seq to the data
    seq = seq.replace("\n","")
    seq = seq.replace("\r","")
    return seq

DNA = read_seq("NCBI-NM_207618.2_DNA.txt")

plt = translate() #removing extra characters, so as to be able to translate only the data

table = {
    'ATA':'I', 'ATC':'I', 'ATT':'I', 'ATG':'M',
    'ACA':'T', 'ACC':'T', 'ACG':'T', 'ACT':'T',
    'AAC':'N', 'AAT':'N', 'AAA':'K', 'AAG':'K',
    'AGC':'S', 'AGT':'S', 'AGA':'R', 'AGG':'R',
    'CTA':'L', 'CTC':'L', 'CTG':'L', 'CTT':'L',
    'CCA':'P', 'CCC':'P', 'CCG':'P', 'CCT':'P',
    'CAC':'H', 'CAT':'H', 'CAA':'Q', 'CAG':'Q',
    'CGA':'R', 'CGC':'R', 'CGG':'R', 'CGT':'R',
    'GTA':'V', 'GTC':'V', 'GTG':'V', 'GTT':'V',
    'GCA':'A', 'GCC':'A', 'GCG':'A', 'GCT':'A',
    'GAC':'D', 'GAT':'D', 'GAA':'E', 'GAG':'E',
    'GGA':'G', 'GGC':'G', 'GGG':'G', 'GGT':'G',
    'TCA':'S', 'TCC':'S', 'TCG':'S', 'TCT':'S',
    'TTC':'F', 'TTT':'F', 'TTA':'L', 'TTG':'L',
    'TAC':'Y', 'TAT':'Y', 'TAA':'_', 'TAG':'_',
    'TGC':'C', 'TGT':'C', 'TGA':'_', 'TGG':'W',
}

if len(length_seq) % 3 == 0: #check that the length of the sequence is divisible by 3
    pass
else:
    raise ErrorSequenceNotDivisibleBy3

for s in seq:

     #loop over the sequence

        #extract/grab single codon
        #look up the codon and store the single value output of the amino acid string
