import sys
import os
from random import randint
from time import sleep
from playerClass import Player

def write(words):
    for char in words:
        sleep(0.060)
        sys.stdout.write(char)
        sys.stdout.flush()
    sleep(1)
    print("")
p = Player()

os.system('clear')
p.name = raw_input("What is your character's name? ")
p.type = 'none'

while p.type == 'none':
    input = raw_input("What is your class? (Cowboy, Mage, Knight, Cyborg Cowboy, Donkey, Orge): ")

    if input.upper() == "COWBOY" or "MAGE" or "KNIGHT" or "CYBORG COWBOY" or "DONKEY" or "ORGE":
        p.type = input.upper()
        print(p.type)
        break
    else:
        words = "Sorry, not a class. (Maybe check your spelling?)"
        write(words)
        p.type = 'none'

words = "(type help to get a list of actions)\n %s enters the world, searching for adventure!" % (p.name)
write(words)

Commands = {
  'quit': Player.quit,
  'help': Player.help,
  'status': Player.status,
  'rest': Player.rest,
  'explore': Player.explore,
  'flee': Player.flee,
  #'attack': Player.attack,
  }

while(p.health > 0):
    global line

    line = raw_input("> ")
    args = line.split()
    if len(args) > 0:
        commandFound = False
    for c in Commands.keys():
      if args[0] == c[:len(args[0])]:
        Commands[c](p)
        commandFound = True
        break
    if not commandFound:
      words = "%s doesn't understand the suggestion." % p.name
      write(words)

words = """You are dead, and now there is no going back. Sorry :P """
write(words)
sys.exit
