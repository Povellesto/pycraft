import math
from time import sleep
import sys
import os
from functools import reduce

"""
Steps for Factoring Trinomial When the Leading Coefficient is 1:

1) Identify a,b, and c in the trinomial ax2 + bx+c, which will be inputed
2) Write down all factor pairs of c
3) Identify which factor pair from the previous step sums up to b
4) Substitute factor pairs into two binomials
"""

def findFactorPairs(n):
    return [(i, n / i) for i in range(1, int(math.sqrt(math.fabs(n))) + 1) if n % i == 0]

def get_all_factors(n):
    global all_factors
    all_factors = []
    for i in range(1,n+1):
        if n%i == 0:
            factors.append(i)
    return factors




def threec1_factor_trinomial(v1,v2,v3):
    factors = findFactorPairs(int(v3))
    print("Factor Pairs of: " + str(v3) + " are " + str(factors))
    sleep(2)
    sumofpairs = list(map(sum, factors))
    print("The Sums of " + str(v3) + "s factor pairs are: " + str(sumofpairs))
    sleep(2)
    for i in range(len(factors)):
        if int(sumofpairs[i]) == int(v2):
            print("The Answer is: (x+" + str(factors[i][0]) + ")(x+" + str(factors[i][1]) + ")" )
            sleep(5)
            break
        else:
            print("The factor pairs: " + str(factors[i])+ " does not work")
            sleep(2)

def threec1_factor_trinomial_input():
    os.system('clear')
    words_3 = ("""ax^2 + bx + c
Where a = 1""")
    for char in words_3:
        sleep(0.070)
        sys.stdout.write(char)
        sys.stdout.flush()
    sleep(2)
    print("")
    v1 = 1
    v2 = input("B = ")
    v3 = input("C = ")

    threec1_factor_trinomial(v1,v2,v3)
"""
Steps for Factoring Trinomial When the Leading Coefficient is not 1:

Step 1: Make sure that the trinomial is written in the correct order;
the trinomial must be written in descending order from highest power to
lowest power. In this case, the problem is in the correct order.

Step 2: Decide if the three terms have anything in common, called the
greatest common factor or GCF. If so, factor out the GCF. Do not forget
to include the GCF as part of your final answer. In this case, the three
terms only have a 1 in common which is of no help.

Step 3: Multiply the leading coefficient and the constant, that is multiply
 the first and last numbers together.

Step 4: List all of the factors from Step 3 and decide which combination of
numbers will combine to get the number next to x. In this case, the numbers 3
and 4 can combine to equal 1.

Step 5: After choosing the correct pair of numbers, you must give each number
a sign so that when they are combined they will equal the number next to x and
also multiply to equal the number found in Step 3.

Step 6: Rewrite the original problem with four terms by splitting the middle
term into the two numbers chosen in step 5.

Step 7: Now that the problem is written with four terms, you can factor by grouping.
"""

def threec_factor_trinomial(v1,v2,v3):
    #check if there are any GFC
    get_all_factors(v1)
    v1FactorList = all_factors
    get_all_factors(v2)
    v2FactorList = all_factors
    get_all_factors(v3)
    v3FactorList = all_factors

    if any(v1FactorList) == v2FactorList and any(v1Factorlist) == v3FactorList:
        #if there is a GFC then devide all values by that GFC, then move on
        pairs1 = set(v1).intersection(set(v2))
        paris2 = set(v1).intersection(set(v3))
        sharedFactors = set(pairs1).intersection(set(pairs2))
        if not sharedFactors:
            print("No Shared Values")
        else:
            print("The greatest common factor is " + str(sharedFactors[-1]))
            v1 = v1/int(sharedFactors[-1])
            v2 = v2/int(sharedFactors[-1])
            v3 = v3/int(sharedFactors[-1])

    #find the factor pairs of the coefficient and the constant

    """
    if abs(v1) + v3 == v2 is True and abs(v1) * v3 == v2 is True:


    elif abs(v1) + abs(v3) == v2 is True and abs(v1) * abs(v3) == v2 is True:
        pass
    elif v1 + v3 == v2 is True and v1 * v3 == v2 is True:

    elif v1 + abs(v3) == v2 is True and v1 * abs(v3) == v2 is True:

    else:
        print("PRIME")

    """

"""
_____________________________________________________________________________________________________________
_____________________________________________________________________________________________________________

"""


def Help():
    os.system('clear')
    words_1 = ("""To select an option within the menu, you must type the corresponding # code
with the option you want to choose. Here is an example:

    1)Add
    2)Subtract
    3)Multiply
    >

To choose what you want to do, you need to enter the number next to what you want to do:

    1)Add
    2)Subtract
    3)Multiply
    > 1

All you need to do is enter in the number, if not the program might not know what you are
asking it todo (no spaces are needed, JUST THE NUMBER)

Extra commands:
"help" - Help
"about" - About Developer

To use these commands enter in the quoted words.

""")
    for char in words_1:
        sleep(0.060)
        sys.stdout.write(char)
        sys.stdout.flush()
    sleep(5)




def menu_choice():
    """ Find out what the user wants to do next. """
    print("Choose one of the following options:")
    print("")
    print("  3c1) Factor Trinomial When the Leading Coefficient is 1")
    print("   3c) Factor Trinomial When the Leading Coefficient is not 1")
    print("  2c1) Factor Binomial When the Leading Coefficient is 1")
    print("   2c) Factor Binomial When the Leading Coeffecient is not 1")
    print(" help) Help")
    print("    q) Quit Program")
    print("")

    choice = raw_input("> ")
    if choice.lower() in ['3c1','3c','2c1','2c','q','help']:
        return choice.lower()
    else:
        print(choice +"?")
        print("Invalid option")
        return None


def main_loop():

    os.system('clear')

    while True:
        os.system('clear')
        choice = menu_choice()
        if choice == None:
            continue
        if choice == 'q':
            print( "Exiting...")
            break     # jump out of while loop
        elif choice == "3c1":
            threec1_factor_trinomial_input()
        elif choice == "3c":
            pass
        elif choice == "2c1":
            pass
        elif choice == "2c":
            pass
        elif choice == "d":
            pass
        elif choice == "help":
            Help()

        else:
            print("Invalid choice.")
            sleep(5)

# The following makes this program start running at main_loop()
# when executed as a stand-alone program.
if __name__ == '__main__':
    main_loop()
