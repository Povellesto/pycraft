"""
Write a simple program which loops over a list of user data (tuples containing a username, email and age)
and adds each user to a directory if the user is at least 16 years old. You do not need to store the age.
Write a simple exception hierarchy which defines a different exception for each of these error conditions:

the username is not unique
the age is not a positive integer
the user is under 16
the email address is not valid (a simple check for a username, the @ symbol and a domain name is sufficient)

Raise these exceptions in your program where appropriate. Whenever an exception occurs,
your program should move onto the next set of data in the list.
Print a different error message for each different kind of exception.

Think about where else it would be a good idea to use a custom class,
and what kind of collection type would be most appropriate for your directory.

You can consider an email address to be valid if it contains one @ symbol and
has a non-empty username and domain name – you don’t need to check for valid characters.
You can assume that the age is already an integer value.
"""
import random
import os

users = ["bob","sal","sal","kale","chips"]
age = []
for i in range(10):
    age.append(random.randrange(1,30,3))

email = ["a@asdf.com","b@asdf.com","c@asdf.com","dasdf.com","e@adsf.com"]
number = 0

class UsernameUniqueError(Exception):
    pass

class AgeNotPositiveError(UsernameUniqueError):
    pass

class UserUnder16Error(UsernameUniqueError):
    pass

class EmailInvalidError(UsernameUniqueError):
    pass

os.system("clear")

while True:
    amount_of_usernames = users.count(number)
    if amount_of_usernames > 1:
        raise UsernameUniqueError("error: Username " + str(users[number]) + " is not Unique")

    if number > 0:
        pass
    elif number == 0:
        pass
    else:
        raise AgeNotPositiveError("error: Invalid Age")

    if age[number] < 16:
        raise UserUnder16Error("error: User " + str(users[number]) + " is Under 16")

    if "@" not in email[number]:
        raise EmailInvalidError("error: Invalide Email")


    number += 1
