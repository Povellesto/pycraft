#Go to https://python-textbok.readthedocs.io/en/1.0/Object_Oriented_Programming.html under Example 1

class Genre:
    def __init__(self, genre):
        self.genre_name = genre
        self.playlists = []

    def add_playlist(self, playlist_name, date_created):
        self.playlists.append(Playlist(playlist_name, date_created ))



class Playlist:
    def __init__ (self, playlist_name, date_created):
        self.name = playlist_name
        self.date = date_created
        self.albums = {}

    def add_album(self, album_name, artist, date_created):
        self.albums[album_name] = Album(album_name, artist, date_created)

class Album:
    def __init__ (self, album_name, artist, date_created):
        self.name = album_name
        self.artist = artist
        self.date_created = date_created
        self.songs = []

    def add_songs(self, song_name):
        self.songs.append(Song(song_name, artist))

class Song:
    def __init__(self, song_name, artist):
        self.name = song_name
        self.artist = artist
        self.songs = []

def func_create_genre():
    global name_g
    name_g = raw_input("What is of the name/genre you want to create: ")
    Genre(name_g)

def func_create_playlist(name_genre):

    name_pl = raw_input("What is the name of the playlist: ")
    date_pl = raw_input("What is the date (M.D.Y): ")
    Genre.add_playlist(name_pl, date_pl)



def func_create_album():
    name_a = raw_input("What is the name of the name: ")
    artist_a = raw_input("What is the artist: ")
    date_a = raw_input("What is the date (M.D.Y): ")


func_create_genre()
func_create_playlist(name_g)

"""
acid_Jazz = Genre("Acid Jazz")
create_pumpknicle = acid_Jazz.add_playlist("Pumpkicle", "8.28.18")

create_album = acid_Jazz.playlists[0].add_album("creativity", "Jokpsed", "9.19.18")
print(acid_Jazz.playlists[0].albums["creativity"].name)
"""
