from Tkinter import *

class Options:
    def __init__(self,game,canvas, paused, fuel_Number, GRAVITY, MAXIMUM_ENGINE_POWER,MAXIMUM_THRUSTER_POWER, window_height, Start_Screen):
        #self.mainmenu = False
        self.game = game#accesing all of the specs in the game class to be the specs in the Lander class.
        self.canvas = game.canvas
        self.title = self.canvas.create_text(350,150, font=("game over", 35), text='OPTIONS')
        self.mainmenu = self.canvas.create_text(35, 490, font=("game over", 12),activefill= 'green', text='MAIN MENU', tag='main')
        self.Video = self.canvas.create_text(350,200, font=("game over", 15), activefill = 'green', text='VIDEO', tag='video')
        self.Gameplay = self.canvas.create_text(350,225, font=("game over", 15), activefill = 'green', text='GAMEPLAY', tag='gameplay')
        self.Controls = self.canvas.create_text(350,250, font=("game over", 15), activefill = 'green', text='CONTROLS/KEYBINDINGS', tag='controls')
        self.About = self.canvas.create_text(350,275, font=("game over", 15), activefill = 'green', text='ABOUT DEVELOPER',tag='about')
        self.canvas.tag_bind('main', '<ButtonPress-1>', self.Start_Screen_Boot)
        self.canvas.tag_bind('about', '<ButtonPress-1>', self.about)
        self.canvas.tag_bind('video', '<ButtonPress-1>', self.video)
        self.canvas.tag_bind('gameplay', '<ButtonPress-1>', self.gameplay)
        self.canvas.tag_bind('controls', '<ButtonPress-1>', self.keybindings)

    def Start_Screen_Boot(self, evt):
        self.game.canvas.delete("all")
        self.mainmenu = True

    def about(self, evt):
        self.game.canvas.delete("all")
        self.title = self.canvas.create_text(60,20, font=("Lombok", 20),fill='blue', text='ABOUT DEV')
        self.about_text = self.canvas.create_text(5,30, font=("courier new", 10), text="""Livi Poon is a student at the Nueva School. He enjoys Astronomy, Cosmology, Climate Science,\nDrawing, Swimming, Programming, Math, and Science.\nHe has participated in two hackathons (at Make Hacks)\nand was nominated the youngest participant two times in a row;\nhe was mentioned in pictures multiple times as well.\nHe also helped to influence the 4th grade curriculum (at The Nueva School)\nby integrating catapult simulations into their\ncomputer science curriculum and creating a whole entire new one week class about programming.\nHe also has worked hard and has finished the college course\nPython Programming: A Concise introduction from Wesleyan University,\nand has worked with a team of students to send an entire experiment to space (Livi as a Project Manager.\nJenko Hwong (Quest) and Dalton Lobo Dias as mentors).
""", anchor = 'nw')
        self.mainmenu = self.canvas.create_text(35, 490, font=("game over", 12),activefill= 'green', text='Main Menu', tag='main')

    def video(self, evt):
        global window_height
        self.game.canvas.delete("all")
        self.title = self.canvas.create_text(40,20, font=("Lombok", 20),fill='blue', text='VIDEO')
        self.mainmenu = self.canvas.create_text(35, 490, font=("game over", 12),activefill= 'green', text='MAIN MENU', tag='main')

        self.height = self.canvas.create_text(144,40, font=("game over", 15), text='Height: %s' % window_height)
        self.height_Plus = self.canvas.create_text(195,34, font=("game over",15), text='|+|', activefill='green', tag='add_window_height')
        self.height_Minus = self.canvas.create_text(195,44, font=("game over",15), text='|-|', activefill='green', tag='minus_window_height')
        self.canvas.tag_bind('add_window_height', '<ButtonPress-1>',self.add_window_height)
        self.canvas.tag_bind('minus_window_height', '<ButtonPress-1>',self.minus_window_height)

        self.width = self.canvas.create_text(144,70, font=("game over", 15), text='Width: %s' % window_width)
        self.width_Plus = self.canvas.create_text(195,64, font=("game over",15), text='|+|', activefill='green', tag='add_window_width')
        self.width_Minus = self.canvas.create_text(195,74, font=("game over",12), text='|-|', activefill='green', tag='minus_window_width')
        self.canvas.tag_bind('add_window_width', '<ButtonPress-1>',self.add_window_width)
        self.canvas.tag_bind('minus_window_width', '<ButtonPress-1>',self.minus_window_width)


    def gameplay(self,evt):
        self.game.canvas.delete("all")
        self.title = self.canvas.create_text(60,20, font=("Lombok", 20),fill='blue', text='GAMEPLAY')
        global fuel_Number
        global GRAVITY
        global ENGINE_POWER
        global THRUSTER_POWER
        global MAXIMUM_ENGINE_POWER
        global MAXIMUM_THRUSTER_POWER
        global L2_engine_down_kb
        global L2_engine_left_kb
        global L2_engine_right_kb
        global L1_engine_left_kb
        global L1_engine_right_kb

        self.fuel = self.canvas.create_text(100,40, font=("game over", 15), text='Fuel: %s' % fuel_Number, anchor ='nw')
        self.fuel_Plus = self.canvas.create_text(195,34, font=("game over",15), text='|+|', activefill='green', tag='add_fuel', anchor='nw')
        self.fuel_Minus = self.canvas.create_text(195,44, font=("game over",15), text='|-|', activefill='green', tag='minus_fuel',anchor='nw')
        self.canvas.tag_bind('add_fuel', '<ButtonPress-1>',self.add_fuel)
        self.canvas.tag_bind('minus_fuel', '<ButtonPress-1>',self.minus_fuel)

        self.gravity = self.canvas.create_text(100,70, font=("game over", 15), text='Power of Gravity: %s' % GRAVITY,anchor='nw')
        self.gravity_Plus = self.canvas.create_text(290,64, font=("game over",15), text='|+|', activefill='green', tag='add_gravity',anchor='nw')
        self.gravity_Minus = self.canvas.create_text(290,74, font=("game over",15), text='|-|', activefill='green', tag='minus_gravity',anchor='nw')
        self.canvas.tag_bind('add_gravity', '<ButtonPress-1>',self.add_gravity)
        self.canvas.tag_bind('minus_gravity', '<ButtonPress-1>',self.minus_gravity)

        self.thruster_p = self.canvas.create_text(100,100, font=("game over", 15), text='Thruster Power: %s' % THRUSTER_POWER,anchor='nw')
        self.thruster_p_Plus = self.canvas.create_text(270,94, font=("game over",15), text='|+|', activefill='green', tag='add_thruster_p',anchor='nw')
        self.thruster_p_Minus = self.canvas.create_text(270,104, font=("game over",15), text='|-|', activefill='green', tag='minus_thruster_p',anchor='nw')
        self.canvas.tag_bind('add_thruster_p', '<ButtonPress-1>', self.add_thruster_p)
        self.canvas.tag_bind('minus_thruster_p', '<ButtonPress-1>', self.minus_thruster_p)

        self.thruster_p = self.canvas.create_text(100,130, font=("game over", 15), text='Thruster Power: %s' % THRUSTER_POWER,anchor='nw')
        self.thruster_p_Plus = self.canvas.create_text(270,124, font=("game over",15), text='|+|', activefill='green', tag='add_thruster_p',anchor='nw')
        self.thruster_p_Minus = self.canvas.create_text(270,134, font=("game over",15), text='|-|', activefill='green', tag='minus_thruster_p',anchor='nw')
        self.canvas.tag_bind('add_thruster_p', '<ButtonPress-1>', self.add_thruster_p)
        self.canvas.tag_bind('minus_thruster_p', '<ButtonPress-1>', self.minus_thruster_p)

        self.mainmenu = self.canvas.create_text(35, 490, font=("game over", 12),activefill= 'green', text='MAIN MENU', tag='main')

    def keybindings(self, evt):
        self.game.canvas.delete("all")
        self.title = self.canvas.create_text(60,20, font=("game over", 20),fill='blue', text='Keybindings')
        self.mainmenu = self.canvas.create_text(35, 490, font=("game over", 12),activefill= 'green', text='MAIN MENU', tag='main')

        self.L1_engine_down = self.canvas.create_text(144,40, font=("game over", 15), text='Fuel: %s' % L1_engine_down_kb)


    def add_fuel(self,evt):
        global fuel_Number
        fuel_Number += 100
        print("")
        print("Fuel.Capacity", fuel_Number)

    def minus_fuel(self,evt):
        global fuel_Number
        fuel_Number -= 100
        print("")
        print("Fuel.Capacity", fuel_Number)

    def add_gravity(self,evt):
        global GRAVITY
        GRAVITY += 0.0001
        print("")
        print("Gravity.Power", GRAVITY)

    def minus_gravity(self,evt):
        global GRAVITY
        GRAVITY -= 0.0001
        print("")
        print("Gravity.Power", GRAVITY)

    def add_thruster_p(self,evt):
        global THRUSTER_POWER
        THRUSTER_POWER += 0.00001
        print("")
        print("Thruster.Power", THRUSTER_POWER)

    def minus_thruster_p(self,evt):
        global THRUSTER_POWER
        THRUSTER_POWER -= 0.00001
        print("")
        print("Thruster.Power", THRUSTER_POWER)

    def add_MAX_thruster_p(self,evt):
        global MAXIMUM_THRUSTER_POWER
        MAXIMUM_THRUSTER_POWER += 0.0001
        print("")
        print("MAX.Thruster.Power", MAXIMUM_THRUSTER_POWER)

    def minus_MAX_thruster_p(self,evt):
        global MAXIMUM_THRUSTER_POWER
        MAXIMUM_THRUSTER_POWER -= 0.0001
        print("")
        print("MAX.Thruster.Power", MAXIMUM_THRUSTER_POWER)

    def controls(self,evt):
        self.game.canvas.delete("all")
        self.mainmenu = self.canvas.create_text(35, 490, font=("game over", 12),activefill= 'green', text='Main Menu', tag='main')

    def add_window_height(self, evt):
        global window_height
        window_height += 10
        print("")
        print("Window.Height" , window_height)

    def minus_window_height(self, evt):
        global window_height
        window_height -= 10
        print("")
        print("Window.Height_" , window_height)

    def add_window_width(self, evt):
        global window_width
        window_width += 10
        print("")
        print("Window.Width_",window_width)

    def minus_window_width(self, evt):
        global window_width
        window_width -= 10
        print ("")
        print("Window.Width_",window_width)
