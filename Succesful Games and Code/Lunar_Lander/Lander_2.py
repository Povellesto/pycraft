from Tkinter import *
import time
import random
import os
#from Lunar_Lander import Start_Screen_Boot

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

class Lander_2:
    def __init__(self, game,canvas,SavedCoords, fuel_Number, paused, lander_2_x1, lander_2_x2, saved_coords, continue_tick):
        self.crash = False
        self.land = False
        game_started= True
        L2_engine_down_kb = '<KeyPress-s>'
        L2_engine_left_kb = '<KeyPress-a>'
        L2_engine_right_kb = '<KeyPress-d>'
        self.game = game#accesing all of the specs in the game class to be the specs in the Lander class.
        self.canvas = canvas
        self.id = self.canvas.create_rectangle(lander_2_x1, 0, lander_2_x2, 20, outline = 'red')
        self.time = time.time()
        self.x = 0
        self.y = 0.01
        self.engine_y = 0
        self.engine_x = 0
        self.fuel = fuel_Number

        self.down = False
        self.left = False
        self.right = False

        self.engine_y_textc = self.canvas.create_text(0, 180,font=("Courier New", 12), text='Main Engine: off', anchor = 'nw')
        self.engine_x_textc = self.canvas.create_text(0, 200,font=("Courier New", 12), text='Thrusters: off', anchor = 'nw')
        self.engine_fuel_textc = self.canvas.create_text(0, 220,font=("Courier New", 12), text='Fuel: %s' % self.fuel,anchor = 'nw')
        self.x_text = self.canvas.create_text(0, 240,font=("Courier New", 12), text='x: %s' % self.x, anchor = 'nw')
        self.y_text = self.canvas.create_text(0, 260,font=("Courier New", 12), text='y: %s' % self.y, anchor = 'nw')

        self.canvas.bind_all(L2_engine_down_kb, self.L2_engine_down)
        self.canvas.bind_all(L2_engine_left_kb, self.L2_engine_left)
        self.canvas.bind_all(L2_engine_right_kb, self.L2_engine_right)

    def Start_Screen_Boot(self, evt):
        self.canvas.delete("all")
        start_screen = Start_Screen(self.game)
        paused = True
        SavedCoords = True

    def L2_engine_down(self, evt):
        if self.down:
            self.canvas.itemconfig(self.engine_y_textc, text='Main Engine: off')
        else:
            self.canvas.itemconfig(self.engine_y_textc, text='Main Engine: on')
        self.Lander_2_engine_down(evt)


    def L2_engine_left(self, evt):
        if self.left:
            self.canvas.itemconfig(self.engine_x_textc, text='Thrusters: off')
        else:
            self.canvas.itemconfig(self.engine_x_textc, text='Thrusters: left')
        self.Lander_2_engine_left(evt)

    def L2_engine_right(self, evt):
        if self.right:
            self.canvas.itemconfig(self.engine_x_textc, text='Thrusters: off')
        else:
            self.canvas.itemconfig(self.engine_x_textc, text='Thrusters: right')
        self.Lander_2_engine_right(evt)

    def move(self, game,paused, SavedCoords, GRAVITY, ENGINE_POWER, THRUSTER_POWER, MAXIMUM_ENGINE_POWER, MAXIMUM_THRUSTER_POWER,lander_2_x1, lander_2_x2, Lunar_2_running): #MOVING THE LANDER FROM SIDE TO SIDE
#        if self.canvas.coords(self.id[3]) >= 480:
#            if self.canvas.coords(self.id[3]) <= 490:
#                print(find_overlapping(self.x1, self.y1, self.x2, self.y2))WD

        if self.canvas.coords(self.id)[3] >= 500:
            self.id = self.canvas.create_rectangle(lander_2_x1, 0, lander_2_x2, 20, outline = 'red')
            if self.y > 1:
                self.crash = True
                continue_tick = 0
            else:
                self.land = True
                continue_tick = 0
            return



        now = time.time()
        time_since_last = now - self.time

        if time_since_last > 0.1:
            if self.down and self.engine_y > MAXIMUM_ENGINE_POWER:
                self.engine_y += ENGINE_POWER

            if self.left and self.engine_x < MAXIMUM_THRUSTER_POWER:
                self.engine_x += THRUSTER_POWER
            elif self.right and self.engine_x > -MAXIMUM_THRUSTER_POWER:
                self.engine_x -= THRUSTER_POWER

            if self.down: #main fuel
                self.fuel -= 2

            if self.left or self.right: #left or right boosters
                self.fuel -= 1

            if self.fuel < 0:
                self.fuel = 0
            self.id_coords = self.canvas.coords(self.id)

            if self.fuel <= 0:
                self.engine_y = 0
                self.engine_x = 0
            self.y = self.y + (time_since_last * GRAVITY) + (time_since_last * self.engine_y)
            self.x = self.x + (time_since_last * self.engine_x)
            self.canvas.move(self.id, self.x, self.y)

            self.canvas.itemconfig(self.engine_fuel_textc, text='Fuel: %s' % self.fuel)
            self.canvas.itemconfig(self.x_text, text='x: %s' % self.x)
            self.canvas.itemconfig(self.y_text, text='y: %s' % self.y)


        self.id_coords = self.canvas.coords(self.id)


    def Lander_2_engine_down(self, evt):
        if self.down:
            self.down = False
            self.engine_y = 0
        else:
            self.down = True


    def Lander_2_engine_left(self, evt):
        if self.left:
            self.left = False
            self.right = False
            self.engine_x = 0
        else:
            self.left = True


    def Lander_2_engine_right(self, evt):
        if self.right:
            self.left = False
            self.right = False
            self.engine_x = 0
        else:
            self.right = True
