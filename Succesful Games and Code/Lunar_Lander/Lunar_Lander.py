from Tkinter import *
import time
import random
import os
import csv
import sys
from Lander_1 import Lander
from Lander_2 import Lander_2
from options import Options


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    END = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    BLINK= '\033[5m'

GRAVITY = 0.0005 #setting up the variables for the lander
ENGINE_POWER = -0.00003
THRUSTER_POWER = 0.00001
MAXIMUM_ENGINE_POWER = -0.002
MAXIMUM_THRUSTER_POWER = 0.0001
game_started = False
paused = False
pause_tick = 0
continue_tick = 0
start_tick = 0
SavedCoords = False
window_height = 500
window_width = 1000
fuel_Number = 2000
Lunar_2_running = False
loading = 0
id1 = 0
id2 = 0
lander_1_WINS = 0
lander_2_WINS = 0
lander_x1 = 290
lander_x2 = 310
lander_2_x1 = 390
lander_2_x2 = 410
running = False
saved_coords = []
saved_coords2 = []

class bcolors:  #setting a class for terminal coloring schemes
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    END = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    BLINK = '\033[5m'


class Game: # the class for the actual game screen
    def __init__(self):
        global window_width
        global window_height
        global running
        self.tk = Tk() #game screen
        self.called_options = None
        self.tk.title("")
        self.canvas = Canvas(self.tk, width=700, height=500, highlightthickness=0)
        self.canvas.pack()
        self.canvas.focus_set()
        self.tk.update()
        self.canvas_height = window_width
        self.canvas_width = window_height
        self.sprites = []
        running = True

    def Start_Screen_Boot(self, paused, SavedCoords):
        self.canvas.delete("all")
        start_screen = Start_Screen(self)
        paused = True
        SavedCoords = True

    def finish(self):
        global paused
        global lander_1_WINS
        global lander_2_WINS
        self.canvas.delete("all")
        paused = True
        if self.sprites[0].crash == True:
            print(bcolors.FAIL + 'BOOM, The Non-Corrupted Lander has crashed!' + bcolors.END)
            self.canvas.delete("all")
            self.crashed = self.canvas.create_text(350,350, font=("Courier New", 24), fill='red', text="LANDER 1 HAS CRASHED")
            self.main = self.canvas.create_text(350,375, font=("Courier New", 12), activefill='green',text="Mainmenu", tag='main')
            self.canvas.tag_bind('main', '<ButtonPress-1>', lambda evt:self.Start_Screen_Boot(paused, SavedCoords))
            lander_2_WINS += 1

        elif self.sprites[1].crash == True:
            print(bcolors.FAIL + 'BOOM, The Corrupted Lander has crashed!' + bcolors.END)
            self.canvas.delete("all")
            self.crashed = self.canvas.create_text(350,350, font=("Courier New", 24), fill='red', text="LANDER 2 HAS CRASHED")
            self.main = self.canvas.create_text(350,375, font=("Courier New", 12), activefill='green',text="Mainmenu", tag='main')
            self.canvas.tag_bind('main', '<ButtonPress-1>', lambda evt:self.Start_Screen_Boot(paused, SavedCoords))
            lander_1_WINS += 1

        elif self.sprites[0].land == True:
            lander_1_WINS += 1
            print(bcolors.HEADER + 'Safe Landing, The Non-Corrupted Lander has Landed!' +  bcolors.END)
            self.canvas.delete("all")
            self.safe = self.canvas.create_text(350,350, font=("Courier New", 24), fill='maroon1', text="LANDER 1 HAS LANDED")
            self.main = self.canvas.create_text(350,375, font=("Courier New", 12), activefill='green',text="Mainmenu", tag='main')
            self.canvas.tag_bind('main', '<ButtonPress-1>', lambda evt:self.Start_Screen_Boot(paused, SavedCoords))

        elif self.sprites[1].land == True:
            lander_2_WINS += 1
            print(bcolors.HEADER + 'Safe Landing, The Corrupted Lander has Landed!' +  bcolors.END)
            self.canvas.delete("all")
            self.safe = self.canvas.create_text(350,350, font=("Courier New", 24), fill='maroon1', text="LANDER 2 HAS LANDED")
            self.main = self.canvas.create_text(350,375, font=("Courier New", 12), activefill='green',text="Mainmenu", tag='main')
            self.canvas.tag_bind('main', '<ButtonPress-1>', lambda evt:self.Start_Screen_Boot(paused, SavedCoords))


    def mainloop(self):
        global paused
        while 1:#updating the postition of the Lunar Lander
            if paused == False:
                if running == True:
                    if len(self.sprites) >= 2:
                        self.sprites[0].move(self.tk, paused, SavedCoords, GRAVITY, ENGINE_POWER, THRUSTER_POWER, MAXIMUM_ENGINE_POWER, MAXIMUM_THRUSTER_POWER,lander_x1,lander_x2,running)
                        self.sprites[1].move(self.tk,paused, SavedCoords, GRAVITY, ENGINE_POWER, THRUSTER_POWER,MAXIMUM_ENGINE_POWER,MAXIMUM_THRUSTER_POWER,lander_2_x1, lander_2_x2,Lunar_2_running)
                if len(self.sprites) >= 2:
                    if self.sprites[0].crash == True or self.sprites[1].crash == True or self.sprites[0].land == True or self.sprites[1].crash == True:
                        self.finish()
                if self.called_options != None and self.called_options.mainmenu  == True:
                    Start_Screen(self)
            #PUT IN PAUSE FUNCTION IN THIS LOOP
            #Thinking about putting the end message, in this loop. Because then it can look at all of the sprites
            self.tk.update_idletasks()
            self.tk.update()
            time.sleep(0.01)

class OVERALL_GAME:
    def __init__(self, game):
        global L2_engine_down_kb
        global L2_engine_left_kb
        global L2_engine_right_kb
        global L1_engine_left_kb
        global L1_engine_right_kb
        global saved_y
        global saved_fuel
        global saved_engine_y
        global saved_engine_x
        global paused
        self.re_init(game)

    def re_init(self,game):
        self.canvas = game.canvas
        self.pause_tick = 0
        self.continue_tick = 0
        if SavedCoords == False:
            self.fuel = fuel_Number
        else:
            self.fuel = saved_fuel

        self.game = game #the game canvas
        self.canvas = game.canvas
        self.canvas.pack()

        self.canvas_height = window_width
        self.canvas_width = window_height
        self.title_text = self.canvas.create_text(550, 10,font=("game over", 30), text='Lunar Lander', anchor= 'nw')
        self.by = self.canvas.create_text(550,30, font=('game over', 10), text='By Livi Poon', anchor = 'nw')
        self.pause = self.canvas.create_text(685, 490,font=("Courier New", 12),activefill= 'green', text='||',  fill = "black", tag='pause')
        self.canvas.tag_bind('pause', '<ButtonPress-1>', self.Pause)
        self.player1 = self.canvas.create_text(0,20, font=("Courier New", 14), text='Player 1', fill="blue", anchor= 'nw')
        self.player2 = self.canvas.create_text(0,160, font=("Courier New", 14), text='Player 2', fill="red", anchor= 'nw')

        lander_1 = Lander(self.game,self.canvas,SavedCoords, fuel_Number, paused, lander_x1, lander_x2, saved_coords, continue_tick)
        lander_2 = Lander_2(self.game,self.canvas,SavedCoords, fuel_Number, paused, lander_2_x1, lander_2_x2, saved_coords2, continue_tick)
        self.game.sprites = []
        self.game.sprites.append(lander_1)
        self.game.sprites.append(lander_2)

    def mainloop(self): #updating the postition of the Lunar Lander
        game.mainloop()

    def Continue(self, game, SavedCoords):
        self.pause_tick = 0
        global running
        global paused
        global Lunar_2_running
        SavedCoords = True
        self.game.canvas.delete("all")
        self.re_init(self.game)
        paused = False
        Lunar_2_running = True
        running = True

    def Start_Screen_Boot(self, evnt, paused, SavedCoords):
        global running
        self.game.canvas.delete("all")
        start_screen = Start_Screen(self.game)
        paused = True
        SavedCoords = True
        running = False

    def Main(self, game):
        global paused
        paused = False
        self.Start_Screen_Boot(game,paused,SavedCoords)

    def callback(self, evt):
        global SavedCoords
        global paused
        global running
        global Lunar_2_running
        global pause_tick
        SavedCoords = False
        self.game.canvas.delete("all")
        self.re_init(self.game)
        pause_tick = 0
        paused = False
        Lunar_2_running = True
        running = True

    def Pause(self, game):
        global running
        global pause_tick
        global continue_tick
        pause_tick +=1
        continue_tick +=1
        print(pause_tick)
        global saved_y
        global saved_fuel
        global saved_engine_y
        global saved_engine_x
        global paused
        global saved_coords
        global saved_coords2

        #saved_fuel = self.sprites[0].fuel
        #saved_y = self.sprites[0].y
        #saved_engine_y = self.sprites[0].engine_y
        #saved_engine_x = self.sprites[0].engine_x
        paused = True
        Lunar_2_running = False
        running = False
        if pause_tick == 2:
            pause_tick = 0
            os.system("clear")
            self.canvas.delete("all")
            self.main = self.canvas.create_text(350,275, font=('Courier New', 15), activefill="green",text ='Main Menu',fill="black",tag='main')
            self.main = self.canvas.create_text(350,300, font=('Courier New', 15), activefill="green",text ='Continue',fill="black",tag='continue')
            self.canvas.tag_bind('main', '<ButtonPress-1>', self.Main)
            self.canvas.tag_bind('continue', '<ButtonPress-1>', lambda evt:self.Continue(game,SavedCoords))
        else:
            self.canvas.delete("all")
            self.paused = self.canvas.create_text(350,200, font=("Courier New", 28), fill='maroon1', text="PAUSED")
            self.title_text = self.canvas.create_text(550, 10,font=("Courier New", 20), text='Lunar Lander', anchor= 'nw')
            self.by = self.canvas.create_text(550,30, font=('Courier New', 10), text='By Livi Poon', anchor = 'nw')
            self.pause = self.canvas.create_text(685, 490,font=("Courier New", 12),activefill= 'green', text='||',  fill = "black", tag='pause')
            self.canvas.tag_bind('pause', '<ButtonPress-1>', self.Pause)

class Start_Screen: # The "main menu" screen, which alows you to open options, start the game, and quit.
    def __init__(self, game):
        self.game = game #the game canvas
        self.canvas = game.canvas
        self.canvas.pack()
        self.canvas_height = window_width
        self.canvas_width = window_height
        self.canvas.delete("all")
        self.title = self.canvas.create_text(350,150, font=('game over', 45), text =('LUNAR LANDER'))
        self.dev = self.canvas.create_text(350,475, font=('game over', 15), text =('- LIVI POON -'))
        self.counter1 = self.canvas.create_text(650,14, font=('game over', 35), text =(':'))
        self.counter2 = self.canvas.create_text(625,15, font=('game over', 30), text =(lander_1_WINS), fill = 'blue')
        self.counter3 = self.canvas.create_text(675,15, font=('game over', 30), text =(lander_2_WINS), fill = 'red')


        f = open('log.txt','a')
        f.write('\n' + time.asctime(time.localtime(time.time())))
        f.close()
        global loading
        if loading == 0:
            #Loading...
            time.sleep(2.5)
            print(chr(27) + "[2J")
            print("Loading.")
            time.sleep(2.5)
            print(chr(27) + "[2J")
            print("Loading..")
            time.sleep(2.5)
            print(chr(27) + "[2J")
            print("Loading...")
            time.sleep(2.5)
            print(chr(27) + "[2J")
            print("Loading.")
            time.sleep(2.5)
            print(chr(27) + "[2J")
            print("Loading..")
            time.sleep(2.5)
            print(chr(27) + "[2J")
            print("Loading...")
            time.sleep(2.5)
            print(chr(27) + "[2J")
            print("--------------------------------------------------")
            print("Succesfully Booted - Lunar Lander v.0.1.1.5")# CHANGE EVERY TIME YOU WORK ON THIS PROJECT
            print("Current Time of Boot: " + time.asctime(time.localtime(time.time())))
            print("--------------------------------------------------")
        loading = 1

        self.start = self.canvas.create_text(350,250, font=("game over", 15), activefill="green",text ='START',fill="black",tag='start')
        if continue_tick >= 1:
            self.start = self.canvas.create_text(350,225, font=('game over', 15),activefill="green", text ='COUNTINUE?',fill="black",tag='countinue')
        self.start = self.canvas.create_text(350,275, font=('game over', 15),activefill="green", text ='OPTIONS',fill="black", tag='options')
        self.start = self.canvas.create_text(350,300, font=('game over', 15),activefill="green", text ='QUIT',fill="black",tag='quit')
        self.canvas.tag_bind('start', '<ButtonPress-1>', self.callback)
        self.canvas.tag_bind('countinue', '<ButtonPress-1>', self.callback2)
        self.canvas.tag_bind('quit', '<ButtonPress-1>', self.quit_action)
        self.canvas.tag_bind('options', '<ButtonPress-1>', self.Options_Lander)

    def Options_Lander(self,evt):
        self.canvas.delete("all")
        self.game.called_options = Options(self.game,self.canvas, paused, fuel_Number, GRAVITY, MAXIMUM_ENGINE_POWER,MAXIMUM_THRUSTER_POWER, window_height,Start_Screen)


    def callback(self, evt):
        global SavedCoords
        global paused
        global running
        global Lunar_2_running
        SavedCoords = False
        self.game.canvas.delete("all")
        self.new_game = OVERALL_GAME(self.game)
        paused = False
        Lunar_2_running = True
        running = True


    def callback2(self, evt):
        global paused
        global SavedCoords
        global running
        global Lunar_2_running
        self.game.canvas.delete("all")
        self.new_game = OVERALL_GAME(self.game)
        paused = False
        Lunar_2_running = True
        running = True

    def quit_action(self, evt):
        self.game.tk.destroy()

g = Game()
start_screen = Start_Screen(g)
g.mainloop()
