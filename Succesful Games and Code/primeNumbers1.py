
# Python program to check if the input number is prime or not

# take input from the user
num = int(input("Enter a number: "))

# prime numbers are greater than 1
if num > 1:
   # check for factors
   for i in range(2,num):
       if (num % i) == 0:
           print(num,"is not a prime number")
           print(i, "times", num//i, "is", num)
           break
   else:
       print(num,"is a prime number")
       print("Because it is only devisible by its self and one")
       
# if input number is less than
# or equal to 1, it is not prime
# this message came from the nano editor
else:
   print(num,"is not a prime number")
   
