from Tkinter import *
import time
import os
import sys
import getpass

#MAKE SURE TO RUN IN PYTHON 3

num_house = 1130072165
houses_per_sec = 1
_time_ = 0
gameStart = False
canvas = 0
houses_completed = 0
current_upgrade_cost = 2000000

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    OKRED =  '\033[31m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    END = '\033[0m'
    BLINK= '\033[5m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    RFGGBG = '\033[1;31;42m'

def menu_choice():
    """ Find out what the user wants to do next. """
    os.system("clear")
    print(bcolors.BOLD + '\033[49;91m' + "Santa Sim 0.0.7 - Game Jam" + bcolors.END)
    print("   s) Start(Collect)"+ bcolors.END)
    print('\033[49;91m' + "   u) Upgrades" + bcolors.END)
    print("   h) Help(How to Play)")
    print('\033[49;91m' + "   q) Quit" + bcolors.END)
    print(bcolors.BOLD + "   d) DEV_ONLY" + bcolors.END)

    choice = raw_input(": ")
    if choice.lower() in ['s','u','d','h','q']:
        return choice.lower()
    else:
        print(choice +"?")
        print(bcolors.WARNING + bcolors.BOLD + "Invalid option" + bcolors.END)
        time.sleep(2.5)
        return None

def start():
    num_house = 1130072165
    _time_ = 0
    global houses_completed
    global houses_per_sec
    global houses_per_sec
    while True:
        os.system("clear")
        num_house -= houses_per_sec
        houses_completed += houses_per_sec
        _time_ += 1
        print("Current Time: ", _time_)
        print("Current Houses Per Second: ", houses_per_sec)
        print("Current Houses Left: ", num_house)
        print("Current Houses Completed: ", houses_completed)
        time.sleep(0.005)
        if _time_ == 43200:
            break

    if houses_completed != 1130072165:
        print("Oh No! You have not completed giving out all the presents! Go back and upgrade you giving powers!!!!!")
        print("You were: ",1130072165 - houses_completed,"away")
        time.sleep(8)

    if houses_completed == 1130072165 or houses_completed >= 1130072165:
        print(bcolors.BOLD + "YOU HAVE SUCCESFULLY DELIVERED ALL THE PRESENTS" + bcolors.END)
        time.sleep(8)


def dev():
    global houses_per_sec
    global houses_completed
    global num_house
    global current_upgrade_cost
    os.system("clear")
    pswd = getpass.getpass('Password:')
    if pswd == "roygibiv":
        time.sleep(1)
        print("- Hello Dev -")
        _1_ = input("Changing Houses Per Second: ")
        houses_per_sec = int(_1_)
        time.sleep(1)
        _2_ = input("Changing Houses Completed: ")
        houses_completed = int(_2_)
        time.sleep(1)
        _3_ = input("Changing Current Upgrade Cost: ")
        current_upgrade_cost = int(_3_)
        time.sleep(8)
        print("")
        print("Current Houses Per Second: ", houses_per_sec)
        print("Current Houses Completed: ", houses_completed)
        print("Current Upgrade Cost: ", current_upgrade_cost)
        time.sleep(8)

    elif pswd != "roygibiv":
        print(bcolors.BOLD + bcolors.OKRED + "INCORRECT PASSWORD")
        time.sleep(5)

def _help_():
    os.system("clear")
    print("Santa Sim 0.0.7 - Game Jam - HELP")
    print("The point of the game is to be able to deliver \nall of the presents in 12 hours (assuming Christmas is on the Winter Solstice) \nYou do this by buying upgrades and watching that counter go down!\nIn this game a second is 0.005 seconds in real life, and game time is 3.525min")
    time.sleep(15)

def upgrades():
    global current_upgrade_cost
    global houses_completed
    global houses_per_sec
    os.system("clear")
    print("Santa Sim 0.0.7 - Game Jam - Upgrades")
    print("To Upgrade Your Houses Per Second You Need: ", current_upgrade_cost)
    if houses_completed <= current_upgrade_cost:
        print("Sadly you do not have enough to upgrade!")
        print("Do you want to collect more houses? (y or _ )")
        choice = input(": ")
        if choice == "y":
            start()
    if houses_completed == current_upgrade_cost or houses_completed >= current_upgrade_cost:
        print(bcolors.BOLD+ "Houses Per Second UPGRADED"+bcolors.END)
        houses_per_sec += 100
        houses_completed -= current_upgrade_cost
        current_upgrade_cost += 2500000
        print("Current House Completed: ", houses_completed)
        print("Current Upgrade Cost: ", current_upgrade_cost)
        time.sleep(8)

def mainloop():
    while True:
        menu_choice()
        choice = menu_choice()
        if choice == None:
            continue
        if choice == 's':
            start()
        elif choice == 'u':
            upgrades()
        elif choice == 'h':
            _help_()
        elif choice == 'd':
            dev()
        elif choice == 'q':
            os.system('clear')
            break

if __name__ == '__main__':
    mainloop()
