from tkinter import*
import importlib
import math
X_START = 0# the x point furthest left
X_END = 800 # the x point furthest right
X_SCALE = 1#1280/60 # how many pixels per x
X_STEP_SIZE = 0.4 # how often to draw lines. The bigger this is, the less smooth the lines look
WIDTH = (X_END-X_START) * X_SCALE # size of the canvas, to make sure the whole x axis fits

Y_START = -400 # the lowest y point
Y_END = 400 # the highest y point
Y_SCALE = 1#760/60 # how many pixels per y
HEIGHT = (Y_END-Y_START) * Y_SCALE # size of the canvas, to make sure the whole y axis fits

GRAPHING_DELAY = 45 # how many milliseconds to wait before drawing the n
T_START = 0
T_END = 60
T_STEP_SIZE = 0.4
#p = 0
def convert_x(x):
    return (x-X_START) * X_SCALE

# shift and scale y so that the whole graph is displayed on the canvas (top left of canvas is (0,0))
def convert_y(y):
    return HEIGHT - ((y-Y_START) * Y_SCALE)
#vfy = viy + ay*t
#vfx = vix + ax*t
def make_quad(ay, ax, vix, viy):
    return lambda t: (vix*t + 0.5*ax*t**2,viy*t + 0.5*ay*t**2)
 #For password lock, 5 times wrong closes the app.
class Catapult(Frame):
       # def log_in():
            #print(chr(27) + "[2J")
            #print('\033[93m' + "Welcome to Catapult.sim" + '\033[0m')
            #for i in range(1):
                #password_command= input('\033[93m' + "To continue please type in your password: " + '\033[0m')
                #if password_command == "askAway1919":
                    #root.destroy(self)
                #else:
                    #print(password_command + " Is not a password.")
                #i +=1
                #if i == 5:
                    #root.destroy()
        #    p =+ 1
        #    if p == 5:
        #        root.destroy()
    #log_in()


    def draw_line(self, x0, y0, x1, y1, color="blue"):
        #('converted {0},{1} to {2},{3}'.format(convert_x(x0), convert_y(y0), convert_x(x1), convert_y(y1)))
        self.w.create_line(convert_x(x0), convert_y(y0), convert_x(x1), convert_y(y1), fill=color)
        #self.w.create_line(x0, y0, x1, y1, fill=color)

    # Draw a line between the next two points. If there are still more points, schedule the next one to be drawn.
    # This animates the drawing of the line instead of drawing it all at once
    def draw_next_line(self):

        index = 0
        while index < len(self.in_process):
            current_t, current_function = self.in_process[index]
            # if x is at the end of the graph, we are done graphing this line
            if current_t >= T_END:
                self.in_process.pop(index) # leave the index the same, since we're deleting the current one
            else :
                t0 = current_t
                t1 = current_t + T_STEP_SIZE
                x0, y0 = current_function(t0)
                x1, y1 = current_function(t1)
                #print('{0},{1} to {2},{3}'.format(x0, y0, x1, y1))
                self.draw_line(x0, y0, x1, y1)

                # update to the next t
                self.in_process[index][0] = t1

                # move on to the next line
                index += 1
        # If there is still more to draw, call this method again to draw the next line after a delay
        if len(self.in_process) > 0:
            self.w.after(GRAPHING_DELAY, self.draw_next_line)


    def draw_axes(self):
        self.w.create_line(0,400, 800, 400, fill="blue", dash=(4, 4))
        self.w.create_line(400,0, 400, 800, fill="blue", dash=(4, 4))

    # remove all items from the graph and redraw the axes
    def erase_graph(self):
        self.w.delete(ALL)
        self.draw_axes()

    # Set the function to be graphed, start x at the beginning, and then draw the first line (if it isn't
    # already in the process of drawing things
    def graph(self, function):
        self.in_process.append([T_START, function])
        print("yes it is, yay")
        if len(self.in_process) == 1:
            print("yes it is")
            self.draw_next_line()

    def password1 (self):
        password = input("Enter Password: ")
        if password == "askAway1919":
            self.create_gameOp


    def create_gameOp(self):
        root2 = Tk()
        root2.config(width=200, height=200)
        self.w = Canvas(root2, width=800, height=800,bg="lightblue")
        self.w.grid(row=0,column=0)
        self.erase_graph()


        #All the input and values for drawing on the graph.
        root3 = Tk()
        w2 = Canvas(root3, width=200, height=200)
        root3.config(width=200, height=200)
        clear= Button(root3, text="Clear", command=self.erase_graph)
        clear.grid(row=5,column=2)
        w2 = Label(root3, text="Initial horizontal Velocity")
        w2.grid(row=1, column=1)

        vix = Entry(root3)
        vix.grid(row=1, column=2)
        angle_launch_label = Label(root3, text="Angle of Launch")
        angle_launch_label.grid(row=2, column=1)
        angle_entry = Entry(root3)
        angle_entry.grid(row=2, column=2)
        ay = Label(root3, text="Veritcal Velocity")
        ay.grid(row=3,column=1)
        self.viy = Entry(root3)
        self.viy.grid(row=3,column=2)
        horizontal_velocity_label= Label(root3, text="Horizontal Velocity")
        horizontal_velocity_label.grid(row=4,column=1)
        self.ax = Entry(root3)
        self.ax.grid(row=4, column=2)
        #This determines if you are using airresistance or not, it also includes the equation need for airresistance.
        def say():
            acelleration = -9.8
            if self.airresistance1 == True:
                velocity = (int(self.viy.get())**2) * math.sqrt(int(self.ax.get()))
                k = 0.000000000001
                acelleration = (- k * abs(velocity) * velocity) + acelleration
            print(self.airresistance1)
            self.graph(make_quad(acelleration,
                                                  int(self.ax.get()),
                                                  int(self.viy.get()),
                                                  int(vix.get())))

            #print('{0} + {1} + {2} + {3}'.format(vix,viy,ay,ax))
        def airresistance():
            print("airresistance has been added")
            self.airresistance1 = True
        b = Button(root3, text="Submit", command=say)
        b.grid(row=5,column=1)
        b = Button(root3, text="Air Resistance", command= airresistance)
        b.grid(row=6,column=1)
        b = Button(root3, text="Add Air Resistance", command= airresistance)
        b.grid(row=6,column=2)

    #def start_items(self):
        #Label(self, text="First Name").pack()
        #Label(self, text="Last Name").pack()
        #3e1 = Entry(self)
        #e2 = Entry(self)

        #e1.pack()
        #e2.pack()
        #Air Resistance code and equation

    def create_Options(self):
        #Creates second window in first screen that has options
        self.text1 = Label(self,text="Welcome to the options!")
        self.text2 = Label(self,text="This part of the app is still in development.")
        self.text1.grid(row=1,column=1)
        self.text2.grid(row=2,column=1)
        self.button1 = Button(self, text="Volume - Not Working")
        self.button2 = Button(self, text="Starting Items - Not Working", command = self.start_items)
        self.button1.grid(row=3,column=1)
        self.button2.grid(row=4,column=1)

    def log(self):
        root5= Tk()
        text1= Label(text= "Hello Zero, welcome to you log!")
        text1.grid(row=1,column=1)
    def check_users(self, entry):
        a = entry.get()
        b = "ZerO"
        if a == b:
            print("Hello ZerO")
            print("Opening Log .....")
            self.log()

    def submit_button(self, entry): #This helps run the entire code, that draws the line onto the graph.
        self.submit = Button(text="Submit", command=  lambda: self.check_users(entry))
        #self.submit.grid()

    def login_screen(self):

        password= Entry(self)
        #password.pack()
        self.submit_button(password)
    def exit(self):
        self.destroy()
    def createWidgets(self):
#

        self.text1 = Label(self,text="Catapult.Sim ")
        self.text1.grid(row=1,column= 1)
        #Text on first page
        self.mb=  Menubutton ( self, text="Menu", relief=RAISED )
        self.mb.grid(row=3, column = 1)  #p = Button(klsdjflkjdlsf)
        self.mb.menu  =  Menu ( self.mb, tearoff = 0)
        self.mb["menu"]  =  self.mb.menu
        # Scroll Bar
        #Scroll bar Stop
        StartVar  = 0
        OptionsVar  = 0
#        Exit = root.destroy()
        self.mb.menu.add_checkbutton ( label="Start", command= self.create_gameOp)
        self.mb.menu.add_checkbutton ( label="Log", command = self.login_screen)
        self.mb.menu.add_checkbutton ( label="Options", command = self.create_Options)
        self.mb.menu.add_checkbutton (label="Exit", command=self.exit)
        self.mb.menu.add_checkbutton (label="Exit", command=self.exit)
        self.mb.grid(row=3, column=1)

    def __init__(self, master= None, on_reload= None): #Runs all the code.
        Frame.__init__(self,master)
        self. master = master
        self.grid(row=0,column=0)
        self.createWidgets()
        self.in_process = []
        self.my_reload = Button(self)
        self.my_reload["text"] = "ReLaunch",
        self.my_reload["command"] = on_reload
        self.my_reload.grid(row=2,column=1)
        self.airresistance1 = False
