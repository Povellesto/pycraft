from tkinter import*
import importlib
import Catapult_Simulator


class Executor:
    def __init__(self):
        self.root = Tk()
        self.app = Catapult_Simulator.Catapult(master=self.root, on_reload=self.on_reload)
        self.app.mainloop()

    def on_reload(self):
        self.root.destroy()

        importlib.reload(Catapult_Simulator)

        self.root = Tk()
        self.app = Catapult_Simulator.Catapult(master=self.root, on_reload=self.on_reload)
        self.app.mainloop()


if __name__ == '__main__':
    Executor()
