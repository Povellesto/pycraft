# Message Sender
import os
from socket import *
host = "127.0.0.1" # set to IP address of target computer
port = 13000
buf = 1024
addr = (host, port)
UDPSock = socket(AF_INET, SOCK_DGRAM)

while True:
    data = raw_input("Enter message to send or type 'break': ")
    UDPSock.sendto(data, addr)
    if data == "break":
        break

    (data_s, addr) = UDPSock.recvfrom(buf)
    print "Received message: " + data
    if data_s == "break":
        break

UDPSock.close()
os._exit(0)
