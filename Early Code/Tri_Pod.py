from tkinter import *

class Hello(Frame):

    def welcomeScreen(self):
        self.grid1 = Label(self.master,text= "Welcome")
        self.welcome.pack()

    def drawCell(canvas, row, col):
        margin = 5
        cellSize = 15
        left = margin + col * cellSize
        right = left + cellSize
        top = margin + row * cellSize
        bottom = top + cellSize
        canvas.create_rectangle(left, top, right, bottom, fill="white")


    def loadTetrisBoard(canvas):
        tetrisBoard = [ [0,0,0,0,0,0,0,0],
                        [0,0,0,0,0,0,0,0],
                        [0,0,0,0,0,0,0,0],
                        [0,0,0,0,0,0,0,0],
                        [0,0,0,1,2,0,0,0],
                        [0,0,0,3,4,0,0,0],
                        [0,0,0,0,0,0,0,0],
                        [0,0,0,0,0,0,0,0]
                      ]

        rows = canvas.data["rows"]
        cols = canvas.data["cols"]
        canvas.data["tetrisBoard"] = tetrisBoard

    def drawTetrisBoard(canvas):
        tetrisBoard = canvas.data ["tetrisBoard"]
        rows = len(tetrisBoard)
        cols = len(tetrisBoard[0])
        for row in range(rows):
            for col in range(cols):
                    drawCell(canvas, row, col)
    def run (rows, cols):
        margin = 5
        cellSize = 15
        canvasWidth = 2*margin + cols*cellSize
        canvasHeight = 2*margin + rows*cellSize
        root = Tk()
        canvas = Canvas(root, width=canvasWidth, height=canvasHeight)
        canvas.pack()
        root.resizable(width=0, height=0)
        canvas.data = { }
        canvas.data ["rows"] = rows
        canvas.data ["cols"] = cols
        init(canvas, rows, cols)
        tetrisBoard = canvas.data ["tetrisBoard"]
        drawTetrisBoard(canvas)
if __name__ == '__main__':
        app = Hello(Tk())
        app.mainloop()
