# Python program to find the H.C.F of two input number

# define a function
def computeHCF(x, y):
   """This function takes two
   integers and returns the H.C.F"""

   # choose the smaller number
   if x > y:
       smaller = y
   else:
       smaller = x

   for i in range(1,smaller + 1):
       if((x % i == 0) and (y % i == 0)):
           hcf = i
   return hcf

num1 = input("!!!Please make X your bigger side!!! X: ")
num2 = input("Y: ")

# take input from the user
# num1 = int(input("Enter first number: "))
# num2 = int(input("Enter second number: "))

print("The G.C.F. of", num1,"and", num2,"is", computeHCF(num1, num2))
print("Therefore there are ", (num1 + num2) - computeHCF(num1, num2), "Squares Shaded")
